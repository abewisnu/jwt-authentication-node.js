
![Logo](https://jwt.io/img/pic_logo.svg)


# JWT Authentication Node.js

This is JWT Authentication Implementation in Node.js created as learning or master rest api server.

To install project folder:

## Installation

Install npm
```bash
npm install
```

Install yarn
```bash
yarn install
```

Copy `.env.example` to `.env`
```bash
DB_HOST=localhost
DB_DATABASE=youdatabase
DB_USERNAME=username
DB_PASSWORD=password
JWT_SECRET="YouSecretKey"
```
Note:- You can use any random string as `JWT_SECRET`


## Running 
To run project, run the following command

```bash
  npm start
```


## API Reference

#### Register

```http
POST /api/register
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `name` | `string` | **Required**. full name |
| `email` | `string` | **Required**. Email Address |
| `password` | `string` | **Required**. password |

#### Login
```http
POST /api/login
```
| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `email` | `string` | **Required**. Email Address |
| `password` | `string` | **Required**. password |


#### User
```http
  GET /api/user
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `api_key` | `string` | **Required**. Your API key |

#### Logout

```http
  GET /api/logout
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `api_key` | `string` | **Required**. Your API key |


## Register
```bash
http://localhost:3000/api/register
```
![App Screenshot](https://img001.prntscr.com/file/img001/pRACtQPIR-WaZqyIZeIE3A.png)

## Login
```bash
http://localhost:3000/api/login
```
![App Screenshot](https://img001.prntscr.com/file/img001/s5-v6lfTRHygw9C8UWqTzg.png)

## user
```bash
http://localhost:3000/api/user
```
![App Screenshot](https://img001.prntscr.com/file/img001/ymGRr962RAqN73O9O-PoYQ.png)

## Logout
```bash
http://localhost:3000/api/logout
```
![App Screenshot](https://img001.prntscr.com/file/img001/e24YSYpyS9a-5JFlj2Jlzw.png)
## Tech Stack

**Server:** Node Js - Express

**Database:** Mysql


## Authors

- [@abewisnu](https://www.gitlab.com/abewisnu)


## License

[MIT](https://choosealicense.com/licenses/mit/)

