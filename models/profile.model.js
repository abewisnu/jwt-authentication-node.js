// profile.model.js
const { DataTypes } = require('sequelize');
const sequelize = require('./connection');

const Profile = sequelize.define('Profile', {
    birthdate: {
        type: DataTypes.DATE,
        allowNull: true
    },
    address: {
        type: DataTypes.TEXT,
        allowNull: true
    },
    photo: {
        type: DataTypes.BLOB,
        allowNull: true
    }
}, {
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    tableName: 'profile'
});

module.exports = Profile;