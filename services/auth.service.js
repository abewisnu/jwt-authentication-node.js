// auth.service
const UserModel = require('../models/user.model');
const ProfileModel = require('../models/profile.model');
const cacheUtil = require('../utils/cache.util');

exports.createUser = (user) => {
    return UserModel.create(user);
}

exports.createProfile = async (id) => {
    try {
        const profile = await ProfileModel.create({
            id: id,
            created_at: new Date(),
            updated_at: new Date()
        });
        return profile;
    } catch (error) {
        console.error("Error creating profile:", error); // Add this error logging
        throw error;
    }
}


exports.findUserByEmail = (email) => {
    return UserModel.findOne({
        where: {
            email: email
        }
    })
}

exports.findUserById = (id) => {
    return UserModel.findByPk(id);
}

exports.logoutUser = (token, exp) => {
    const now = new Date();
    const expire = new Date(exp * 1000);
    const milliseconds = expire.getTime() - now.getTime();
    /* ----------------------------- BlackList Token ---------------------------- */
    return cacheUtil.set(token, token, milliseconds);
}